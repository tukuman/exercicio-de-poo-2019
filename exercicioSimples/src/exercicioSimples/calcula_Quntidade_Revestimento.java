package exercicioSimples;
//L� as dimens�es das paredes de um c�modo, calcula e escreve a qantidade de caixas de azulejo 
//necess�rias para revesti-las (n�o ser� descontada a �rea ocupada por portas e janelas).
//Cada caixa de azulejos possui 1,5 m2.

import java.text.DecimalFormat;
import java.util.Scanner;

public class calcula_Quntidade_Revestimento {
	
	//Objetos
	static Scanner input = new Scanner (System.in);
	static DecimalFormat decimal = new DecimalFormat("##0.0");
	
	//Declara��o de vari�veis
	static double comprimento, largura, altura; 
	
	//Fun��o
	public static double numero_caixas() {
		return (((comprimento + largura) *2) * altura) / 1.5;
	}
	public static void main(String[] args) {	
		
		//Entrada de dados
		System.out.print("\n\n * Informe as medidas do c�modo * \n\n");
		
		System.out.print(" Altura do p� direito: ");
		altura = input.nextDouble();
		
		System.out.print("\n Comprimento: ");
		comprimento = input.nextDouble();
		
		System.out.print("\n Largura: ");
		largura = input.nextDouble();
				
		//Sa�da de dados
		System.out.println(" Ser�o necess�rias " + decimal.format(numero_caixas()) + " caixas de azulejo.");
	}
}
