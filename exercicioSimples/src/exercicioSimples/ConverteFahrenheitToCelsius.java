package exercicioSimples;

import java.text.DecimalFormat;
import java.util.Scanner;

public class ConverteFahrenheitToCelsius {
	static Scanner input = new Scanner(System.in);
	static DecimalFormat decimal = new DecimalFormat("###,##0.0");
	        // # - significam o numero de posi��es disponiveis para serem usadas com algarismos.
	        // caso o numero de algarismos n�o completem as posi��es disponiveis, estas n�o ser�o 
	        // preenchidas com zeros.(exemplo: 9.9  /  365.5).
			// J� no 0(zero), caso n�o exista algarismos para substitui-lo, se tornar� visivel.
			// (exemplo: 9.0 /  87.0  /  0.6 / ).
	
	//Declara��o de Variaveis
	static double temperatura, celsius;
	
	//Fun��o
		public static double ConverteTemperatura() {
			return ((temperatura - 32) * 5) / 9;		
		}

	public static void main(String[] args) {
		
		
		
		//Entrada de Dados
		System.out.print("\n Informe a temperatura em Fahrenheit: ");
		temperatura = input.nextDouble();
		
		//Processo
		celsius = ConverteTemperatura();
		
		//Saida de informa��o formatada.
		//A vari�vel foi formatada dentro do objeto decimal
		System.out.println("\n A temperatura em Celsius �: "+ decimal.format(celsius));	
	}
}

