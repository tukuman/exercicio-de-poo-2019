package exercicioSimples;

import java.text.DecimalFormat;
import java.util.Scanner;

public class ConverteCelsiusToFahrenheit {
	static  Scanner input = new Scanner(System.in);
	static DecimalFormat decimal = new DecimalFormat("##0.0");
	//Declara��o de Variaveis
	static double temperatura, Fahrenheit;
	
	// Fun��o
	public static double ConverteTemperatura() {
		return (temperatura * 9/5) + 32;
	}

	public static void main(String[] args) {
   		
		//Entrada de Dados
		System.out.println(" Informe a a temperatura em Celsius: ");
		temperatura = input.nextDouble();
		
		//Processo
		Fahrenheit = ConverteTemperatura();
		
		//Sa�da da informa��o
		System.out.println(" \n A temperatura em Fahrenheit �: "+ decimal.format(Fahrenheit));

	}

}


