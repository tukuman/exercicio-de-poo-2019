package exercicioSimples;

import java.util.Scanner;

public class CalculaNumeroDeLampadas {
	
	static Scanner input = new Scanner(System.in);
	
	//Declara��o de Variavel
	static double LarguraComodo, ComprimentoComodo, PotenciaLampada;
	static int NumeroLampada;
	
	//Func�o
		public static int CalculaNumeroLampada() {
		  return  (int) ((18 * (LarguraComodo * ComprimentoComodo)) / PotenciaLampada);
		}
			
	public static void main(String[] args) {
		
		//Entrada de Dados
		System.out.print("\n\n Qual a pot�ncia da lampada (em watts)? ");
		PotenciaLampada = input.nextDouble();
		
		System.out.print("\n Qual a largura docomodo (em metros)? ");
		LarguraComodo = input.nextDouble();
		
		System.out.print("\n Qual o comprimento do comodo (em metros)? ");
		ComprimentoComodo = input.nextDouble();
		
		//Processo
		NumeroLampada = CalculaNumeroLampada ();
				
		//Sa�da de Informa��o
		System.out.println("\n\n S�o necessarias " + NumeroLampada +" lampadas para iluminar esse comodo");
	}
}




		